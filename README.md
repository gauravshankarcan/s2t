# Tower Onboarding Automation

This tower automation is an ansible play to load onboard ansible projects .

The Play can be used to onboard other ansible play following a specific set of best practices . The project provides a base template on how to create an onboarding project , this templates need to be modified to suit your organizations needs

## Setup
Onboard the play onto ansible tower in an admin group
Update the local file  woth the variable which work for your environment , and for those like git url etc which needs to be input by the user, create a survey to override the defaults.

Any variable in the local file is basically exposed for surveys. also acts as inventory 

##### Note
As this is a template, more can be added to ths project to even dynamically build surveys for onboarded plays.


## Execution:

Run on any ansible engine worker , with following variables entered in as survery or as extra variables
```yaml
        tower_token: < obtained for tower user , it should have access to write into the org your onboarding the project to >
        project_scm_url: < git url >
        git_user: < git user >
        git_password: < git password >
        project_scm_branch: < git branch >
        git_inventory_path: < inventory file >
        tower_org: < org to onboard>
        tower_project: < project name >
````

Execute the play and watch the magic happen where ansible auto builds the tower project for you .
